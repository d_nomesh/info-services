package info.mubasher.infoservices.bootstrap;

import info.mubasher.infoservices.model.Narrative;
import info.mubasher.infoservices.repositories.NarrativeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class BootStrapData implements CommandLineRunner {

    private final NarrativeRepository narrativeRepository;

    public BootStrapData(NarrativeRepository narrativeRepository) {
        this.narrativeRepository = narrativeRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Loading Narrative Data....");
        Narrative n1 = new Narrative();
        n1.setId(111111);
        n1.setMixId(209225588);
        n1.setPublishDate(new Date());
        narrativeRepository.save(n1);

        Narrative n2 = new Narrative();
        n2.setId(222222);
        n2.setMixId(209224400);
        n2.setPublishDate(new Date());
        narrativeRepository.save(n2);

        System.out.println("Narratives Saved: "+ narrativeRepository.count());

    }
}
