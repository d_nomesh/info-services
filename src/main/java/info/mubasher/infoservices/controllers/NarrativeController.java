package info.mubasher.infoservices.controllers;

import info.mubasher.infoservices.model.Narrative;
import info.mubasher.infoservices.services.NarrativeService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(NarrativeController.BASE_URL)
public class NarrativeController {
    public static final String BASE_URL="/api/v1/narrative";

    private NarrativeService narrativeService;

   // HttpServletResponse res;

    public NarrativeController(NarrativeService narrativeService) {
        this.narrativeService = narrativeService;
    }

    @GetMapping
    public List<Narrative> listAllNarrative(HttpServletRequest request, HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin","*");
        return narrativeService.findAllNarrative();
    }

    @GetMapping("/{id}")
    public Narrative findNarrativeBySequenceId(@PathVariable Long id){
       // res.addHeader("Access-Control-Allow-Origin","*");
        return narrativeService.findNarrativeBySequenceId(id);
    }

}
