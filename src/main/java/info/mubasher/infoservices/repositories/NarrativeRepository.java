package info.mubasher.infoservices.repositories;

import info.mubasher.infoservices.model.Narrative;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NarrativeRepository extends JpaRepository<Narrative, Long> {
    @Query(value ="select n.id, n.mixId, n.publishDate from narrative n order by n.id desc limit 100", nativeQuery = true)
    List<Narrative> findAll();
}
