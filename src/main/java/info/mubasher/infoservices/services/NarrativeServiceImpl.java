package info.mubasher.infoservices.services;

import info.mubasher.infoservices.model.Narrative;
import info.mubasher.infoservices.repositories.NarrativeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NarrativeServiceImpl implements NarrativeService {

    private final NarrativeRepository narrativeRepository;

    public NarrativeServiceImpl(NarrativeRepository narrativeRepository) {
        this.narrativeRepository = narrativeRepository;
    }

    @Override
    public List<Narrative> findAllNarrative() {
        return narrativeRepository.findAll();
    }

    @Override
    public Narrative findNarrativeBySequenceId(Long id) {
        return narrativeRepository.findById(id).get();
    }

    @Override
    public Optional<Narrative> findNarrativeByMixId(Long mixId) {
        return narrativeRepository.findById(mixId);
    }
}
