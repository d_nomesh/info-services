package info.mubasher.infoservices.services;

import info.mubasher.infoservices.model.Narrative;

import java.util.List;
import java.util.Optional;

public interface NarrativeService {

    List<Narrative> findAllNarrative();

    Narrative findNarrativeBySequenceId(Long id);

    Optional<Narrative> findNarrativeByMixId(Long mixId);

}
