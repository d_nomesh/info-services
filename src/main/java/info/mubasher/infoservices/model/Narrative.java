package info.mubasher.infoservices.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

@Table(name = "narrative")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Narrative implements Serializable {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @Column(name="mixid", nullable = true, unique = true)
    private Long mixId;

    @Column(name = "publishdate", nullable =true)
    private Date publishDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMixId() {
        return mixId;
    }

    public void setMixId(long mixId) {
        this.mixId = mixId;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }


}
