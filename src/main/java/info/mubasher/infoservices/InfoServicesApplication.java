package info.mubasher.infoservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoServicesApplication.class, args);
	}

}

